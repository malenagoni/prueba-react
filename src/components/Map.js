import React, { useRef, useEffect, useContext } from "react";
import mapboxgl from "mapbox-gl";
import { BUILDINGS_ON_SALE_GEOJSON_SOURCE } from "../data";
import { MapContext } from "../Context/map-context";

const mapContainerStyle = {
    height: "100%",
    width: "100%",
};

mapboxgl.accessToken = "pk.eyJ1IjoiY2FydG9tZXRyaWNzIiwiYSI6ImNqOGJ2ZXIzazAxd3kyd3AyMDVrOGpzNWkifQ.KwvwFfoDOeLnjR1gEHO8tg";

function Map(props) {
    const map = useRef(null);
    const mapContainer = useRef(null);
    const { setPanelProperties, setShowPanel, panelProperties, setShowForm } = useContext(MapContext);

    /* UseEffect: Loads the map when the Map component is mounted */
    useEffect(() => {
        /* Create a map instance in a specific HTML element container (referenced by
		the mapContainer useRef), using a specific style (the basemap), and with other
		configuration parameters like the initial center coordinates and zoom level */
        map.current = new mapboxgl.Map({
            container: mapContainer.current,
            style: "mapbox://styles/mapbox/satellite-v9",
            center: [-3.695, 40.4496],
            zoom: 15,
        });

        // Add map navigation controls (zoom and rotation)
        map.current.addControl(new mapboxgl.NavigationControl(), "top-right");

        /* When the map style (basemap) loads, then we can load our sources (geospatial data),
    layers (visual representations of that geospatial data over the map)
    and layers' event handlers */
        map.current.on("style.load", () => {
            // Add source
            map.current.addSource("parcels-for-sale-source", {
                type: "geojson",
                data: BUILDINGS_ON_SALE_GEOJSON_SOURCE,
            });

            // Add layer
            map.current.addLayer({
                id: "parcels-for-sale-layer",
                type: "fill",
                source: "parcels-for-sale-source",
                paint: {
                    "fill-color": "rgb(255, 185, 0)",
                    "fill-opacity": 0.9,
                    "fill-outline-color": "rgb(38, 247, 202)",
                },
            });

            // Add layer mousemove event handler to change cursor to pointer when hovering
            // over a parcel on sale
            map.current.on("mousemove", "parcels-for-sale-layer", () => {
                map.current.getCanvas().style.cursor = "pointer";
            });

            // Add layer mouseleave event handler to change cursor back to normal
            map.current.on("mouseleave", "parcels-for-sale-layer", () => {
                map.current.getCanvas().style.cursor = "";
            });

            // Add layer click event handler
            map.current.on("click", "parcels-for-sale-layer", (e) => {
                const clickedFeature = e.features[0];
                // console.log("Clicked feature: ", clickedFeature);
                // console.log("Clicked feature properties: ", clickedFeature.properties);

                /***************************
				CONTINUE WORKING HERE...
				***************************/

                // First time opening panel and setting properties
                if (panelProperties.refcat.length === 0) {
                    setPanelProperties(clickedFeature.properties);
                    setShowPanel((prev) => !prev);

                    // If it's a different building
                } else if (panelProperties?.refcat !== clickedFeature.properties.refcat) {
                    setPanelProperties(clickedFeature.properties);
                    setShowPanel(true);
                    setShowForm(false);

                    // If it's the same building
                } else if (panelProperties?.refcat === clickedFeature.properties.refcat) {
                    setShowPanel((prev) => !prev);
                    setShowForm(false);
                }
            });
        });
    });

    return <div ref={mapContainer} style={mapContainerStyle}></div>;
}

export default Map;
