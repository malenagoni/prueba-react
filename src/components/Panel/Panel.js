import React, { useContext, useState } from "react";
import { postForm } from "../../actions/actions";
import { MapContext } from "../../Context/map-context";
import "./Panel.css";

function Panel() {
    const { panelProperties, showPanel, showForm, setShowForm, setShowPanel } = useContext(MapContext);
    const { refcat, contacto, lastVisit, description, price } = panelProperties;
    const [input, setInput] = useState({
        nombreApellido: "",
        telefono: "",
        correo: "",
        precio: "",
        refcat,
    });
    function onClick() {
        setShowForm(true);
        setShowPanel(false);
    }
    function handleSubmit(e) {
        e.preventDefault();
        setShowForm(false);
        setShowPanel(false);
        postForm(input);
        setInput({
            nombreApellido: "",
            telefono: "",
            correo: "",
            precio: "",
            refcat,
        });
    }
    function handleChange(e) {
        setInput({
            ...input,
            [e.target.name]: e.target.value,
            refcat,
        });
    }
    return (
        <>
            {showPanel && (
                <div className="Panel">
                    <div className="Panel-Title">INFORMACIÓN BÁSICA DE PARCELA</div>
                    <div className="Panel-Data">
                        <div className="Panel-Data-Sub">REFCAT:</div>
                        <div>{refcat}</div>
                        <div className="Panel-Data-Sub">CONTACTO:</div>
                        <div>{contacto}</div>
                        <div className="Panel-Data-Sub">ÚLTIMA VISITA:</div>
                        <div>{lastVisit}</div>
                        <div className="Panel-Data-Sub">DESCRIPCIÓN:</div>
                        <div>{description}</div>
                        <div className="Panel-Data-Sub">PRECIO DE VENTA:</div>
                        <div>{price}</div>
                    </div>
                    <button className="Panel-Btn" onClick={onClick}>
                        comprar
                    </button>
                </div>
            )}
            {showForm && (
                <div className="Panel">
                    <div className="Panel-Title">INFORMACIÓN BÁSICA DE PARCELA</div>
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <div className="Panel-Data">
                            <label>NOMBRE Y APELLIDO DEL COMPRADOR:</label>
                            <input name="nombreApellido" onChange={handleChange} value={input.nombreApellido} required />
                            <label>TELÉFONO:</label>
                            <input name="telefono" onChange={handleChange} value={input.telefono} required />
                            <label>CORREO ELECTRÓNICO:</label>
                            <input name="correo" onChange={handleChange} value={input.correo} required />
                            <label>PRECIO DE COMPRA:</label>
                            <input name="precio" onChange={handleChange} value={input.precio} required />
                        </div>
                        <button className="Panel-Btn" type="submit">
                            enviar
                        </button>
                    </form>
                </div>
            )}
        </>
    );
}

export default Panel;
