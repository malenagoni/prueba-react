import React, { useState } from "react";

export const MapContext = React.createContext();

export const MapProvider = ({ children }) => {
    const [panelProperties, setPanelProperties] = useState({
        refcat: "",
        contacto: "",
        lastVisit: "",
        description: "",
        price: "",
    });
    const [showPanel, setShowPanel] = useState(false);
    const [showForm, setShowForm] = useState(false);
    return (
        <MapContext.Provider
            value={{
                panelProperties,
                setPanelProperties,
                showPanel,
                setShowPanel,
                showForm,
                setShowForm,
            }}
        >
            {children}
        </MapContext.Provider>
    );
};
