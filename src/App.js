import React from "react";
import Map from "./components/Map";
import Panel from "./components/Panel/Panel";
import { MapProvider } from "./Context/map-context";

function App(props) {
    return (
        <MapProvider>
            <div style={{ display: "flex", flexDirection: "row", width: "100vw", height: "100vh" }}>
                <Map />
                <Panel />
            </div>
        </MapProvider>
    );
}

export default App;
