import axios from "axios";

export const url = `http://localhost:8000`;

export const postForm = (data) => {
    const { nombreApellido, telefono, correo, precio, refcat } = data;
    
    if (nombreApellido && telefono && correo && precio && refcat) {
        return async () => {
            const response = axios({
                method: "post",
                url: `${url}/sales`,
                data: {
                    nombreApellido,
                    telefono,
                    correo,
                    precio,
                    refcat,
                },
            });
            return response;
        };
    }
};
